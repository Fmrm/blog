import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { BlogComponent } from './components/blog/blog.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { PaginaComponent } from './components/pagina/pagina.component';
import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { ErrorComponent } from './components/error/error.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'blog',
        component: BlogComponent
    },
    {
        path: 'formulario',
        component: FormularioComponent
    },
    {
        path: 'peliculas',
        component: PeliculasComponent
    },
    {
        path: 'pagina-de-pruebas',
        component: PaginaComponent
    },
    {
        path: 'pagina-de-pruebas/:nombre/:apellido',
        component: PaginaComponent
    },
    {
        path: '**',
        component: ErrorComponent
    },


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
