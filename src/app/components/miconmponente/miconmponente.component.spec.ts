import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiconmponenteComponent } from './miconmponente.component';

describe('MiconmponenteComponent', () => {
  let component: MiconmponenteComponent;
  let fixture: ComponentFixture<MiconmponenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiconmponenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiconmponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
