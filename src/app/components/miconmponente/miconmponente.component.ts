import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-miconmponente',
  templateUrl: './miconmponente.component.html',
  styleUrls: ['./miconmponente.component.css']
})
export class MiconmponenteComponent implements OnInit {
  mostrarpeliculas: boolean;
  constructor() {
    this.mostrarpeliculas = true;
   }

  ngOnInit() {

  }

  ocultarPeliculas() {
    this.mostrarpeliculas = false;
  }

}
