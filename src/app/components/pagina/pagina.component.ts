import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';



@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  nombre: string;
  apellido: string;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((paramas: Params) => {
      this.nombre = paramas.nombre;
      this.apellido = paramas.apellido;
    });
  }

  redireccion() {
    this.router.navigate(['pagina-de-pruebas', 'Martin', 'Fierro']);
  }

}
