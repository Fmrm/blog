import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
export interface Pelicula { title: string; image: string; year: number; }

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  @Input() pelicula: Pelicula;
  @Output() marcarFavorita = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  seleccionar(event: any, pelicula: any) {
    this.marcarFavorita.emit({
      pelicula
    });
  }

}
