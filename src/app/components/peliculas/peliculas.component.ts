import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
export interface Pelicula { title: string; image: string; year: number; }

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit, DoCheck, OnDestroy {

  peliculas: Pelicula[];
  titulo: string;
  favorita: Pelicula;
  fecha: any;

  constructor() {

    this.titulo = 'Componente peliculas';
    console.log('constructor lanzado');

    this.peliculas = [
      {
        year: 2019,
        title: 'Spiderman',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTnSzTMBZuNxw0XYZnUIGZy12kCOsprRXlujB2gkTD_MSLawSV&s'
      },
      {
        year: 2018,
        title: 'Vengadores',
        image: 'https://i.ytimg.com/vi/fI_CqtIr2hg/maxresdefault.jpg'
      },
      {
        year: 2015,
        title: 'Iron-Man',
        image: 'https://images.clarin.com/2019/07/19/iron-man-el-superheroe-de___DFNW49cvT_1256x620__1.jpg'
      },
      {
        year: 2016,
        title: 'Iron-Man 2',
        image: 'https://cdn-ssl.s7.disneystore.com/is/image/DisneyShopping/7745055551383'
      },
    ];
  }

  ngOnInit() {
    console.log('Componente Inciado');
    this.fecha = new Date(2020, 0, 21);
  }

  ngDoCheck() {
    console.log('Docheck lanzado');
  }

  cambiarTitulo() {
    this.titulo = 'El titulo ha sido cambiado!';
  }

  ngOnDestroy() {
    console.log('El componente se va a eliminar');
  }

  mostrarFavorita(event) {
    this.favorita = event.pelicula;
  }

}
